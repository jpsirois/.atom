
0.4.0 / 2014-05-14 
==================

 * adding support for windows and linux

0.3.0 / 2014-03-17
==================

 * bug added ;)
 * adding the option to hide error panel on no errors
 * updating [atom-message-panel](https://github.com/tcarlsen/atom-message-panel) to 0.3.0

0.2.0 / 2014-03-11
==================

 * adding `validateOnSave` and `validateOnChange` options
 * new and better way to display the error
 * now using [atom-message-panel](https://github.com/tcarlsen/atom-message-panel)
